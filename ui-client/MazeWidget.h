#ifndef MAZEWIDGET_H
#define MAZEWIDGET_H

#include <QWidget>
#include <QPaintEvent>
#include "GameState.h"

class MazeWidget : public QWidget{
	Q_OBJECT
public:
	MazeWidget( IGame* iGame);
	bool isExit();
private:
	QImage getImage( QString imageName);
	IGame* m_igame;
	int m_lengthX;
	int m_lengthY;
	Coordinats m_userCoordinates;
	
	void paintEvent( QPaintEvent*);
	
	int m_width;
	int m_height;
	
	int m_cellWidth;
	int m_cellHeight;
	
	QMap< QString, QImage> m_mapWithImages;
	
	void imagesToMap();
	
	void keyPressEvent(QKeyEvent *event);
	
	void actionForStep( SideOfWorld worldSideForNextStep);

	bool m_exitFromApp;
	Coordinats getCoordinatesToMove( SideOfWorld moveSide);
	};

#endif // MAZEWIDGET_H
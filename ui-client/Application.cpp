#include "Application.h"
#include <QtDebug>
#include "MainWindow.h"
#include <QVBoxLayout>

Application::Application(){
	setCentralWidget( &m_stackedWidget);
	
	MainWindow mainWindow;
	m_stackedWidget.addWidget( &mainWindow);
	m_stackedWidget.setCurrentWidget( &mainWindow);
	
	QVBoxLayout* mainLayout = new QVBoxLayout();
	mainLayout->addWidget( &m_stackedWidget);
	setLayout( mainLayout);
	
}


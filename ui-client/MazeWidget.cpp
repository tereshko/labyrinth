#include "MazeWidget.h"
#include <QPainter>
#include <QResource>
#include "Colors.h"
#include "DoorKey.h"
#include "Door.h"
#include <QMap>
#include <QMessageBox>
#include <QMetaObject>

MazeWidget::MazeWidget( IGame* iGame) {
	imagesToMap();
	m_igame = iGame;
	m_exitFromApp = false;
}

void MazeWidget::keyPressEvent(QKeyEvent *key_event){
	switch (key_event->key()) {
		case Qt::Key_Up:
			{
				actionForStep(SIDE_NORTH);
				repaint();
				break;
			}
		case Qt::Key_Down:
			{
				actionForStep(SIDE_SOUTH);
				repaint();
				break;
			}
		case Qt::Key_Left:
			{
				actionForStep(SIDE_WEST);
				repaint();
				break;
			}
		case Qt::Key_Right:
			{
				actionForStep(SIDE_EAST);
				repaint();
				break;
			}
	}
}

Coordinats MazeWidget::getCoordinatesToMove( SideOfWorld moveSide) {
	Coordinats coordinatsToMove;
	int userY = m_igame->getUserProfile().m_userCoordinats.coordinatY;
	int userX = m_igame->getUserProfile().m_userCoordinats.coordinatX;
	switch (moveSide) {
		case SIDE_NORTH: {
			userY -= 1;
			coordinatsToMove.coordinatX = userX;
			coordinatsToMove.coordinatY = userY;
			break;
		}
		case SIDE_EAST: {
			userX += 1;
			coordinatsToMove.coordinatX = userX;
			coordinatsToMove.coordinatY = userY;
			break;
		}
		case SIDE_SOUTH: {
			userY += 1;
			coordinatsToMove.coordinatX = userX;
			coordinatsToMove.coordinatY = userY;
			break;
		}
		case SIDE_WEST: {
			userX -= 1;
			coordinatsToMove.coordinatX = userX;
			coordinatsToMove.coordinatY = userY;
			break;
		}
		default:{
			break;
		}
	}
	return coordinatsToMove;
}

void MazeWidget::actionForStep( SideOfWorld worldSideForNextStep){
	CellValues valueForNextStep;

	Coordinats coordinatsForNextStep;
	coordinatsForNextStep = getCoordinatesToMove( worldSideForNextStep);
	valueForNextStep = m_igame->getMaze().getCellValueFromCoordinates( coordinatsForNextStep.coordinatX, coordinatsForNextStep.coordinatY);
	
	if( valueForNextStep == DOOR) {
		//get doorColor
		Colors doorColor;
		Thing* nextObj = m_igame->getMaze().getObjByCoordinates( coordinatsForNextStep.coordinatX, coordinatsForNextStep.coordinatY);
		Door* objForDoorColor = dynamic_cast<Door*>( nextObj);
		doorColor = objForDoorColor->getColor();

		//check inventory
		int inventoryCount = m_igame->getUserProfile().getCountOfInventory();
		if( inventoryCount > -1) {
			bool isDoStep = false;
			while (inventoryCount >= 0 && !isDoStep) {
				Thing* inventoryObj = nullptr;
				inventoryObj = m_igame->getUserProfile().getInventoryByIndex(inventoryCount);
				CellValues cellValue;
				cellValue = inventoryObj->getType();
				if( cellValue == CellValues::DOOR_KEY) {
					DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(inventoryObj);
					if( objForDoorKeyColor) {
						Colors keyColor;
						keyColor = objForDoorKeyColor->getColor();
						if( doorColor == keyColor) {
							isDoStep = m_igame->doStep(worldSideForNextStep, OPEN_DOOR, inventoryCount);
						}
					}
				}
				inventoryCount--;
			}
		}
	}
	bool isStepNeeded = true;
	if( valueForNextStep == EXIT){
		switch( QMessageBox::question( this, tr("Maze"), tr("Exit?"), 
		QMessageBox::Yes | 
		QMessageBox::No | 
		QMessageBox::Cancel, QMessageBox::Cancel) ) {
			case QMessageBox::Yes:
				isStepNeeded = true;
				m_exitFromApp = true;
				break;
			case QMessageBox::No:
				isStepNeeded = false;
				break;
			case QMessageBox::Cancel:
				isStepNeeded = false;
				break;
			default:
				isStepNeeded = false;
				break;
		}
	}
	if( isStepNeeded) {
		bool isMoved = m_igame->doStep(worldSideForNextStep, MOVE, -1);
		if( m_exitFromApp){
			QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
		}
	}
}

void MazeWidget::paintEvent( QPaintEvent*){
	// берем размер моего maze
	m_lengthX = m_igame->getMaze().getLengthX();
	m_lengthY = m_igame->getMaze().getLengthY();

	// где сохранен юзер
	m_userCoordinates = m_igame->getUserProfile().getUserCoordinats();
	
	// индекс юзера
	int userIndex = m_userCoordinates.coordinatY * m_lengthX + m_userCoordinates.coordinatX;
	
	// размер виджета
	m_width = this->width();
	m_height = this->height();
	
	// высота моих квадратиков
	m_cellWidth = m_width / m_lengthX;
	m_cellHeight = m_height / m_lengthY;
	
	QPainter painter;
	painter.begin(this);
	painter.setRenderHint( QPainter::Antialiasing, true);
	
	for (int i = 0; i < m_lengthX; i++) {
		for (int j = 0; j < m_lengthY; j++) {
			int x = m_cellWidth * i;
			int y = m_cellHeight*j;
			if( userIndex == j*m_lengthX +i){
				painter.drawImage( x, y, getImage("keanu"));
			} else {
				CellValues cellValue;
				Thing* objByCoordinates;
				objByCoordinates = m_igame->getMaze().getObjByCoordinates(i, j);
				cellValue = objByCoordinates->getType();
				switch (cellValue) {
					case EMPTY:
						{
							painter.drawImage( x, y, getImage("floorImage"));
							break;
						}
					case WALL:
						{
							painter.drawImage( x, y, getImage("wallImage"));
							break;
						}
					case DOOR:
						{
							Door* objForDoorColor = dynamic_cast<Door*>(objByCoordinates);
							if( objForDoorColor){
								Colors colorInObj;
								colorInObj = objForDoorColor->getColor();
								bool dooOpened = objForDoorColor->canPass();
								if( dooOpened){
									switch (colorInObj) {
										case BLACK:
											{
												painter.drawImage( x, y, getImage("doorBlackOpened"));
												break;
											}
										case RED:
											{
												painter.drawImage( x, y, getImage("doorRedOpened"));
												break;
											}
										case GREEN:
											{
												painter.drawImage( x, y, getImage("doorGreenOpened"));
												break;
											}
									}
								} else{
									switch (colorInObj) {
										case BLACK:
											{
												painter.drawImage( x, y, getImage("doorBlackClosed"));
												break;
											}
										case RED:
											{
												painter.drawImage( x, y, getImage("doorRedClosed"));
												break;
											}
										case GREEN:
											{
												painter.drawImage( x, y, getImage("doorGreenClosed"));
												break;
											}
									}
								}
							}
							break;
						}
					case DOOR_KEY:
						{
							DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(objByCoordinates);
							if( objForDoorKeyColor) {
								Colors colorInObj;
								colorInObj = objForDoorKeyColor->getColor();
								switch (colorInObj){
									case BLACK:
										{
											painter.drawImage( x, y, getImage("keyBlack"));
											break;
										}
									case RED:
										{
											painter.drawImage( x, y, getImage("keyRed"));
											break;
										}
									case GREEN:
										{
											painter.drawImage( x, y, getImage("keyGreen"));
											break;
										}
									}
								}
							break;
							}
					case EXIT:
						{
							painter.drawImage( x, y, getImage("mazeExit"));
							break;
						}
					case OUT_OF_MAZE:
						{
							painter.drawImage( x, y, getImage("outOfMaze"));
							break;
						}
					default:
						{
							painter.drawImage( x, y, getImage("no_entry"));
							break;
						}
				}
			}
		}
	}
	painter.end();
}

QImage MazeWidget::getImage( QString imageName){
	QImage imageFromMap = m_mapWithImages.value(imageName);
	if( imageFromMap.isNull()){
		imageFromMap = m_mapWithImages.value("noImage");
	}
	imageFromMap = imageFromMap.scaled( m_cellWidth, m_cellHeight, Qt::IgnoreAspectRatio);
	return imageFromMap;
}

void MazeWidget::imagesToMap() {
	QImage noEntry( ":/errors/no_entry");
	QImage outOfMaze( ":/errors/out");
	QImage mazeExit( ":/env/door_exit");
	QImage noImage(":/errors/no-image");
	
	QImage keyGreen( ":/keys/key_green");
	QImage keyRed( ":/keys/key_red");
	QImage keyBlack( ":/keys/key_black");
	
	QImage doorBlackClosed( ":/doors_closed/door_closed_black");
	QImage doorRedClosed( ":/doors_closed/door_closed_red");
	QImage doorGreenClosed( ":/doors_closed/door_closed_green");
	
	QImage doorBlackOpened( ":/doors_open/door_opened_black");
	QImage doorGreenOpened( ":/doors_open/door_opened_green");
	QImage doorRedOpened( ":/doors_open/door_opened_red");
	
	QImage keanu( ":/players/keanu");
	QImage wallImage( ":/env/wall");
	QImage floorImage( ":/env/floor");

	m_mapWithImages.insert( "no_entry", noEntry);
	m_mapWithImages.insert( "out", outOfMaze);
	m_mapWithImages.insert( "noImage", noImage);
	m_mapWithImages.insert( "mazeExit", mazeExit);
	m_mapWithImages.insert( "keyGreen", keyGreen);
	m_mapWithImages.insert( "keyRed", keyRed);
	m_mapWithImages.insert( "keyBlack", keyBlack);
	m_mapWithImages.insert( "doorBlackClosed", doorBlackClosed);
	m_mapWithImages.insert( "doorRedClosed", doorRedClosed);
	m_mapWithImages.insert( "doorGreenClosed", doorGreenClosed);
	m_mapWithImages.insert( "doorBlackOpened", doorBlackOpened);
	m_mapWithImages.insert( "doorGreenOpened", doorGreenOpened);
	m_mapWithImages.insert( "doorRedOpened", doorRedOpened);
	m_mapWithImages.insert( "keanu", keanu);
	m_mapWithImages.insert( "wallImage", wallImage);
	m_mapWithImages.insert( "floorImage", floorImage);
}

bool MazeWidget::isExit() {
	return m_exitFromApp;
}


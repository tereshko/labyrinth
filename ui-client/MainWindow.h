#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>

class MainWindow : public QWidget{
	Q_OBJECT
public:
	MainWindow();
public slots:
	void gameType( QString typeOfGame);

private:
	QPushButton* m_localGame;
	QPushButton* m_networkGame;
	QPushButton* m_exit;
	
	QVBoxLayout m_mainLayout;
};

#endif // MAINWINDOW_H

#include "Mainwindow.h"
#include <QSignalMapper>
#include "GameState.h"
#include "MazeWidget.h"
#include <QtDebug>
#include "NetworkGame.h"

Mainwindow::Mainwindow(){
	QSignalMapper* mapper = new QSignalMapper(this);
	connect( mapper, SIGNAL( mapped( const QString)), this, SLOT( gameType( QString)));

	m_localGame = new QPushButton("Local Game");
	connect( m_localGame, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping( m_localGame, "local");
	
	m_networkGame = new QPushButton("Network Game");
	connect( m_networkGame, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping( m_networkGame, "network");
	
	m_exit = new QPushButton("Exit");
	connect( m_exit, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping( m_exit, "exit");
	
	//m_mainLayout = new QVBoxLayout();
	m_mainLayout.addWidget( m_localGame);
	m_mainLayout.addWidget( m_networkGame);
	m_mainLayout.addWidget( m_exit);
	m_mainLayout.update();
	setLayout( &m_mainLayout);
}

void Mainwindow::gameType( QString typeOfGame){
	if( typeOfGame == "local"){
		m_localGame->hide();
		m_networkGame->hide();
		m_exit->hide();
		GameState* gameState = new GameState;
		MazeWidget* maze = new MazeWidget( gameState);
		m_mainLayout.addWidget( maze);
		maze->setFocus();
	}
	if( typeOfGame == "network"){
		m_localGame->hide();
		m_networkGame->hide();
		m_exit->hide();
		NetworkGame* networkGame= new NetworkGame;
		MazeWidget* maze = new MazeWidget( networkGame);
		m_mainLayout.addWidget( maze);
		maze->setFocus();
	}
	if( typeOfGame == "exit"){
		
	}
}

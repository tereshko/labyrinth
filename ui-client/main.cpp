#include "MainWindow.h"
#include <QtWidgets>
#include "Application.h"
#include "GameState.h"
#include "MazeWidget.h"

int main (int argc, char** argv) {

	QApplication app(argc, argv);

	MainWindow mainwindow;
	mainwindow.resize(800,800);
	mainwindow.show();
	
	int result = app.exec();

//	if( mazeWidget.isExit()) app.exit();
	return result;
}
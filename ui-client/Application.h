#ifndef APPLICATION_H
#define APPLICATION_H

#include <QMainWindow>
#include <QStackedWidget>

class Application : public QMainWindow{
	Q_OBJECT
public:
	Application();

private:
	QStackedWidget m_stackedWidget;
	};

#endif // APPLICATION_H
#include "NetworkGameServer.h"
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <pthread.h>

struct SOCKET {
	int clientSocket;
	};


void* thread_func(void *arg) {
	int clientSocket;
	bool isGameEnd = false;
	SOCKET* newSocket;
	
	newSocket = static_cast<SOCKET*>(arg);
	clientSocket = newSocket->clientSocket;
	
	delete newSocket;
	
	GameState gameState;
	while( !isGameEnd ) {
		gameState.getMaze().saveMaze( clientSocket);
		gameState.getUserProfile().saveUserProfile( clientSocket);
		
		SideOfWorld sideofWorld;
		TypeOfUserActionsForMove actionTypeForMove;
		int inventoryId;
		
		int sideofWorldReaded = read( clientSocket, &sideofWorld, sizeof (SideOfWorld));
		int actionTypeForMoveReaded =read( clientSocket, &actionTypeForMove, sizeof (TypeOfUserActionsForMove));
		int inventoryIdReaded =read( clientSocket, &inventoryId, sizeof (int));
		
		gameState.doStep( sideofWorld, actionTypeForMove, inventoryId);
		if ( sideofWorldReaded == 0 && actionTypeForMoveReaded == 0 && inventoryIdReaded == 0) {
			close( clientSocket);
			isGameEnd = true;
		}
	}
	close( clientSocket);
}

void NetworkGameServer::serverStart() {
	int sock =0;
	int listener = 0;

	struct sockaddr_in addr;
	listener = socket(AF_INET, SOCK_STREAM, 0);
	if( listener > 0) {
		addr.sin_family = AF_INET;
		addr.sin_port = htons(PORT_USAGE);
		addr.sin_addr.s_addr = htonl(INADDR_ANY);

		if( bind(listener, (struct sockaddr *)&addr, sizeof(addr)) >= 0) {
			while (true) {
				if( listen(listener, 1) == 0) {
					sock = accept(listener, NULL, NULL);
					if ( sock > 0) {
						m_isShutdown = false;
						pthread_t thread;
						
						SOCKET* clientSocketStruct = new SOCKET;
						clientSocketStruct->clientSocket = sock;
						
						pthread_create( &thread, NULL, thread_func, clientSocketStruct);
					} else {
						std::cout << "error. can't await a connection on socket\n";
					}
					m_isShutdown = true;
				} else {
					std::cout << "error. can't prepare to accept connections\n";
				}
			}
		} else {
			std::cout << "error. can't give local address\n";
		}
	} else {
		std::cout << "error. can't open socket\n";
	}
}
#include <iostream>
#include "UserProfile.h"

class UserEditor {
public:
	UserEditor();
	UserProfile* getUserProfile();
	
private:
	UserProfile m_userProfile;

};

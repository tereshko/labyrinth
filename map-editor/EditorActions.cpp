#include "EditorActions.h"
#include "DoorKey.h"
#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


void EditorActions::editMazeSize(){
	std::cout << "\n----------------Maze Resize----------------\n";
	int newLengthX, newLengthY;

	std::cout << "Please enter new Y: ";
	std::cin >> newLengthY;


	std::cout << "Please enter new X: ";
	std::cin >> newLengthX;

	m_mapEditor.getMaze()->resize( newLengthY, newLengthX);
	m_userEditor.getUserProfile()->setUserCoordinates( 0, 0);
	std::cout << "-------------------------------------------\n";
}

void EditorActions::moveUser(){
	std::cout << "\n----------------Move User------------------\n";
	std::cout << "Enter coordinates to move the user\n";
	
	int userY = inputY();
	int userX = inputX();
	bool isCoordinateExit = isCoordinatesExit();
	bool isUserMoveValid = isMoveValid( userX, userY);
	
	if( isUserMoveValid && !isCoordinateExit) {
		m_userEditor.getUserProfile()->m_userCoordinats.coordinatX = userX;
		m_userEditor.getUserProfile()->m_userCoordinats.coordinatY = userY;
		
	}

	std::cout << "-------------------------------------------\n";
}

bool EditorActions::isCoordinatesExit() {
	bool isExit = false;

	Thing* doorObj;
	doorObj = m_mapEditor.getMaze()->getObjByCoordinates(   m_userEditor.getUserProfile()->m_userCoordinats.coordinatX, 
															m_userEditor.getUserProfile()->m_userCoordinats.coordinatY);

	if( doorObj != nullptr) {
		if( doorObj->getType() == CellValues::DOOR){
			isExit = doorObj->canPass();
		}
		if( doorObj->getType() == CellValues::EXIT){
			isExit = true;
		}
	}
	return isExit;
}

int EditorActions::inputY() {
	bool usefullY = false;
	int y = 0;

	while (!usefullY) {
		std::cout << "Enter Y: ";
		std::cin >> y;
		usefullY = isMazeLengthYValid( y);
	}
	return y;
}

int EditorActions::inputX() {
	bool usefullX = false;
	int x = 0;

	while (!usefullX) {
		std::cout << "Enter X: ";
		std::cin >> x;
		usefullX = isMazeLengthXValid( x);
	}
	return x;
}

void EditorActions::changeMaze() {
	std::cout << "\n----------------Change Maze----------------\n";
	int lengthY = inputY();
	int lengthX = inputX();
	showMazeValues();
	CellValues mazeValue = inputValue();
	m_mapEditor.getMaze()->setMatrixValue( lengthY, lengthX, mazeValue);
	std::cout << "-------------------------------------------\n";
}

CellValues EditorActions::inputValue() {
	bool usefullValue = false;
	int inputValue = 0;
	CellValues mazeValueCorrect;

	while (!usefullValue) {
		std::cout << "Please enter value: ";
		std::cin >> inputValue;
		mazeValueCorrect = (CellValues)inputValue;
		usefullValue = isMazeValueValid( mazeValueCorrect);
	}
	return mazeValueCorrect;
}

void EditorActions::showMazeValues(){
	std::cout << CellValues::EMPTY << ". empty\n";
	std::cout << CellValues::WALL << ". wall\n";
	std::cout << CellValues::DOOR << ". door\n";
	std::cout << CellValues::DOOR_KEY << ". door key\n";
	std::cout << CellValues::EXIT << ". exit\n";
}

bool EditorActions::isMoveValid( int userX, int userY) {
	bool canPass = false;
	Thing* obj;
	obj = m_mapEditor.getMaze()->getObjByCoordinates( userX, userY);
	if( obj != nullptr) {
		canPass = obj->canPass();
		if( obj->canPickUp()) {
			m_userEditor.getUserProfile()->addInventoryObj(obj);
			m_mapEditor.getMaze()->setMatrixValue( 
													m_userEditor.getUserProfile()->m_userCoordinats.coordinatY,
													m_userEditor.getUserProfile()->m_userCoordinats.coordinatX,
													CellValues::EMPTY);
		}
	}
	return canPass;
}

bool EditorActions::isMazeValueValid( CellValues value){
	return m_mapEditor.getMaze()->isValueValid( value);
}

bool EditorActions::isMazeLengthYValid( int lengthY){
	return m_mapEditor.getMaze()->isLengthYValid( lengthY);
}

bool EditorActions::isMazeLengthXValid( int lengthX){
	return m_mapEditor.getMaze()->isLengthXValid( lengthX);
}

void EditorActions::printMatrix() {
	std::cout << "----------------Maze-----------------------\n";
	int lengthX = m_mapEditor.getMaze()->getLengthX();
	int lengthY = m_mapEditor.getMaze()->getLengthY();

	Coordinats userCoordinates;
	userCoordinates = m_userEditor.getUserProfile()->getUserCoordinats();

	int userIndex = userCoordinates.coordinatY * lengthX + userCoordinates.coordinatX;
	for (int y = 0; y < lengthY; y++) {
		for (int x = 0; x < lengthX; x++) {
			if( userIndex == y*lengthX +x) {
				std::cout << "X" << " ";
			} else{
				CellValues cellValue;
				cellValue = m_mapEditor.getMaze()->getCellValueFromCoordinates( x, y);
				std::cout << cellValue;
				std::cout << " ";
			}
		}
		std::cout << "\n";
	} 
	std::cout << "\n-------------------------------------------\n";
}

void EditorActions::addInventory(){
	CellValues value;
	value = DOOR_KEY;
	TypesMazeObjects mazeObj;
	Thing* doorKeyObj;
	doorKeyObj = mazeObj.getTypeMazeObject( value);
	DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(doorKeyObj);
	objForDoorKeyColor->setColorForDoorKey();
	m_userEditor.getUserProfile()->addInventoryObj( objForDoorKeyColor);
	
}

void EditorActions::showInventory() {
	std::cout << "---------------Inventory---------------------\n";
	UserProfile::InventoryObj* currentNode;
	currentNode = m_userEditor.getUserProfile()->getFirstInventory();
	if( currentNode == nullptr){
		std::cout << "The inventory list is empty\n";
	} else {
		int invCount = 0;
		Thing* obj;
		CellValues cellValue;
		while ( currentNode != nullptr) {
			obj = currentNode->obj;
			cellValue = obj->getType();

			std::cout << invCount <<" inventory: ";
			printInformationAboutStatusCell( obj);

			if( cellValue == CellValues::DOOR_KEY) {
				Colors colorInObj;
				DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(obj);
				if( objForDoorKeyColor) {
					colorInObj = objForDoorKeyColor->getColor();
					std::cout << ", color: ";
					printInformationColorOfObject( colorInObj);
				}
			}
			currentNode = currentNode->next;
			invCount++;
		}
	}
	std::cout << "\n-------------------------------------------\n";
}

void EditorActions::printInformationAboutStatusCell( Thing* thing){
	if ( thing == nullptr){
		std::cout << "border";
	} else {
		thing->drawName();
	}
}

void EditorActions::printInformationColorOfObject( Colors color) {
	switch (color) {
		case Colors::BLACK:
			std::cout << "black\n";
			break;
		case Colors::GREEN:
			std::cout << "green\n";
			break;
		case Colors::RED:
			std::cout << "red\n";
			break;
		default:
			std::cout << "unknown color\n";
			break;
	}
}

void EditorActions::save(){
	std::string fileName = "editorSave";
	const char* convertedFileName = fileName.c_str();
	int fd = -1;
	fd = openFile( convertedFileName, true);
	m_mapEditor.getMaze()->saveMaze( fd);
	m_userEditor.getUserProfile()->saveUserProfile( fd);
	close( fd);
}

int EditorActions::openFile( const char* fileName, bool isForSave) {
	int fd;
	fd = open( fileName, O_RDWR);
	if( fd == -1) {
		if( isForSave) {
			fd = createFile( fileName);
		}
	}
	return fd;
}

int EditorActions::createFile( const char* fileName) {
	int fd;
	fd = open( fileName, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	return fd;
}
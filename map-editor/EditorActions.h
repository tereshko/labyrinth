#include "GameState.h"
#include "MapEditor.h"
#include "UserEditor.h"

class EditorActions {
public:
	void editMazeSize();
	void moveUser();
	void changeMaze();
	void printMatrix();
	void addInventory();
	void showInventory();
	void save();
private:
	MapEditor m_mapEditor;
	UserEditor m_userEditor;

	bool isMazeValueValid( CellValues value);
	bool isMazeLengthYValid( int lengthY);
	bool isMazeLengthXValid( int lengthX);

	bool isCoordinatesExit();

	int inputY();
	int inputX();
	CellValues inputValue();
	
	
	void showMazeValues();
	bool isMoveValid( int userX, int userY);
	
	void printInformationAboutStatusCell( Thing* thing);
	void printInformationColorOfObject( Colors color);
	
	int openFile( const char* fileName, bool isForSave);
	int createFile( const char* fileName);
	
	
	};
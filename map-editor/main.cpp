#include "EditorActions.h"
#include <iostream>

int main (int argc, char** argv) {

	EditorActions editorActions;
	int userAction;
	bool correctAnswer = false;
	while( !correctAnswer){
		editorActions.printMatrix();
		editorActions.showInventory();
		std::cout << "\n";
		std::cout << "1. Create new map\n";
		std::cout << "2. Move user\n";
		std::cout << "3. Edit map\n";
		std::cout << "4. Add inventory\n";
		std::cout << "5. Save\n";
		std::cout << "6. Exit\n";
		std::cout << "Please, select actions: ";
		std::cin >> userAction;
		switch (userAction) {
			case 1:
				editorActions.editMazeSize();
				break;
			case 2:
				editorActions.moveUser();
				break;
			case 3:
				editorActions.changeMaze();
				break;
			case 4:
				editorActions.addInventory();
				break;
			case 5:
				editorActions.save();
				break;
			case 6:
				correctAnswer = true;
				break;
			default:
				std::cout << "Wrong answer\n";
				break;
		}
	}
	return 0;
}

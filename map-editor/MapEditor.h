#include <Maze.h>

class MapEditor{
public:
	MapEditor();
	Maze* getMaze();
private:
	Maze m_maze;
};
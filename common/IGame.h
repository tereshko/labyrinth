#ifndef IGAME_H
#define IGAME_H

#include "UserProfile.h"
#include "Maze.h"
#include "SideOfWorld.h"
#include "TypeOfUserActionsForMove.h"

class IGame {

public:
	virtual ~IGame() = default;
	virtual bool doStep( SideOfWorld sideOfWorld, TypeOfUserActionsForMove actionTypeForMove, int inventoryId) = 0;
	virtual const UserProfile& getUserProfile() = 0;
	virtual const Maze& getMaze() = 0;

};

#define PORT_USAGE 3410
#endif

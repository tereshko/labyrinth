#include "Maze.h"
#include "Empty.h"
#include "Door.h"
#include "DoorKey.h"

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "Coordinats.h"

Maze::Maze( int lengthY, int lengthX) {
	m_cell = nullptr;
	resize( lengthY, lengthX);
}

Maze::~Maze()
{
	int arraySize = m_lengthX * m_lengthY;
	for(int i = 0; i < arraySize; i ++){
		delete m_cell[i];
	}
	delete [] m_cell;

}

void Maze::resize( int lengthY, int lengthX) {
	if( m_cell != nullptr){
		int arraySize = m_lengthX * m_lengthY;
		for(int i = 0; i < arraySize; i ++){
			delete m_cell[i];
		}
		delete [] m_cell;
	}

	m_lengthX = lengthX;
	m_lengthY = lengthY;

	int arraySize = m_lengthX * m_lengthY;
	m_cell = new Thing*[arraySize];

	for(int i = 0; i < arraySize; i ++)
	{
		m_cell[i] = new Empty;
	}
}

void Maze::saveMaze(int fd) const {
	write( fd, &m_lengthY, sizeof(int));
	write( fd, &m_lengthX, sizeof(int));

	int arraySize = m_lengthX * m_lengthY;

	CellValues cellValue;
	for ( int i = 0; i < arraySize; i++) {
		cellValue = m_cell[i]->getType();
		write( fd, &cellValue, sizeof(CellValues));

		switch (cellValue) {
			case DOOR:{
				Door* objForDoorColor = dynamic_cast<Door*>(m_cell[i]);
				Colors color;
				color = objForDoorColor->getColor();
				write( fd, &color, sizeof(Colors));
				
				bool canPass;
				canPass = objForDoorColor->canPass();
				write( fd, &canPass, sizeof(bool));
				break;
			}
			case DOOR_KEY:{
				DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(m_cell[i]);
				Colors color;
				color = objForDoorKeyColor->getColor();
				write( fd, &color, sizeof(Colors));
				break;
			}
			default:
				break;
		}
	}
}

void Maze::loadMaze( int fd) {
	int lengthY, lengthX;
	read( fd, &lengthY, sizeof( int));
	read( fd, &lengthX, sizeof( int));
	
	resize( lengthY, lengthX);
	int arraySize = lengthX * lengthY;
	
	Thing* mazeObj = nullptr;
	CellValues temp;
	for ( int i = 0; i < arraySize; i++) {
		read( fd, &temp, sizeof( CellValues));
		mazeObj = m_typeMazeObject.getTypeMazeObject( temp);
	
		if( temp == DOOR){
			Colors color;
			bool canPass;
			read( fd, &color, sizeof( Colors));
			read( fd, &canPass, sizeof( bool));
			Door* objForDoorColor = dynamic_cast<Door*>(mazeObj);
			objForDoorColor->setColor( color);
			objForDoorColor->setPass( canPass);
			setObjToMatrix( i, objForDoorColor);
		}
	
		if( temp == DOOR_KEY){
			Colors color;
			read( fd, &color, sizeof( Colors));
			DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(mazeObj);
			objForDoorKeyColor->setColor( color);
			setObjToMatrix( i, objForDoorKeyColor);
		}
		if( temp != DOOR && temp != DOOR_KEY){
			setObjToMatrix( i, mazeObj);
		}
	}
}

int Maze::getLengthY() const {
	return m_lengthY;
}

int Maze::getLengthX() const {
	return m_lengthX;
}

void Maze::setObjToMatrix( int matrixNumber, Thing* obj) {
	m_cell[matrixNumber] = obj;
}

void Maze::setMatrixValue( int y, int x, CellValues value) {
	bool isDataValid = false;
	isDataValid = isEnteredDataValid( y, x, value);

	if( isDataValid){
		int indexForChange = calculateIndex( x, y);
		Thing* obj;
		obj = m_typeMazeObject.getTypeMazeObject( value);
		if( obj != nullptr) {
			if( value == DOOR_KEY){
				DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(obj);
				objForDoorKeyColor->setColorForDoorKey();
				m_cell[indexForChange] = objForDoorKeyColor;
			}
			if( value == DOOR){
				Door* objForDoorColor = dynamic_cast<Door*>(obj);
				objForDoorColor->setColorForDoor();
				m_cell[indexForChange] = objForDoorColor;
			}
			if( value != DOOR && value != DOOR_KEY){
				m_cell[indexForChange] = obj;
			}
		}
	}
}

int Maze::calculateIndex( int coordinateX, int coordinateY){
	int coordinateIndex = coordinateY * m_lengthX + coordinateX;
	return coordinateIndex;
}

bool Maze::isEnteredDataValid( int lengthY, int lengthX, CellValues value){
	return isLengthYValid( lengthY) && isLengthXValid( lengthX) && isValueValid( value);
}

bool Maze::isLengthYValid( int lengthY) const{
	bool isLengthYUsefull = false;
	if ( (0 <= lengthY) && (lengthY < m_lengthY) ) {
		isLengthYUsefull = true;
	}
	return isLengthYUsefull;
}

bool Maze::isLengthXValid( int lengthX) const{
	bool isLengthXUsefull = false;
	if ( ( 0 <= lengthX) && (lengthX < m_lengthX)) {
		isLengthXUsefull = true;
	}
	return isLengthXUsefull;
}

bool Maze::isValueValid( CellValues value) const{
	bool isValueUsefull = false;
	if ( ( EMPTY <= value) && (EXIT >= value)) {
		isValueUsefull = true;
	}
	return isValueUsefull;
}

Thing* Maze::getObjByCoordinates( int x, int y) const {
	int coordinats = (y * m_lengthX) + x;
	Thing* obj = nullptr;
	if( (x >= m_lengthX || y >= m_lengthY || y < 0 || x < 0 || coordinats > ( m_lengthX * m_lengthY) ) ){
		obj = nullptr;
	} else {
		obj = m_cell[coordinats];
	}
	return obj;
}

CellValues Maze::getCellValueFromCoordinates( int x, int y) const {
	CellValues cellValue;
	Thing* obj = nullptr;
	obj = getObjByCoordinates( x, y);
	if( obj == nullptr) {
		cellValue = OUT_OF_MAZE;
	} else{
		cellValue = obj->getType();
	}
	return  cellValue;
}

Maze Maze::getMaze( int fd) {
	Maze createdMaze(0,0);
	int lengthY, lengthX;
	
	read( fd, &lengthY, sizeof( int));
	read( fd, &lengthX, sizeof( int));
	
	createdMaze.resize( lengthY, lengthX);
	int arraySize = lengthX * lengthY;
	
	Thing* mazeObj = nullptr;
	CellValues temp;
	for ( int i = 0; i < arraySize; i++) {
		read( fd, &temp, sizeof( CellValues));
		TypesMazeObjects typeMazeObject;
		mazeObj = typeMazeObject.getTypeMazeObject( temp);
	
		if( temp == DOOR){
			Colors color;
			bool canPass;
			read( fd, &color, sizeof( Colors));
			read( fd, &canPass, sizeof( bool));
			Door* objForDoorColor = dynamic_cast<Door*>(mazeObj);
			objForDoorColor->setColor( color);
			objForDoorColor->setPass( canPass);
			createdMaze.setObjToMatrix( i, objForDoorColor);
		}
	
		if( temp == DOOR_KEY){
			Colors color;
			read( fd, &color, sizeof( Colors));
			DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(mazeObj);
			objForDoorKeyColor->setColor( color);
			createdMaze.setObjToMatrix( i, objForDoorKeyColor);
		}
		if( temp != DOOR && temp != DOOR_KEY){
			createdMaze.setObjToMatrix( i, mazeObj);
		}
	}
	return createdMaze;
}

Maze& Maze::operator=( const Maze &obj) {
	int lengthX, lengthY;
	lengthX = obj.getLengthX();
	lengthY = obj.getLengthY();
	
	this->resize(lengthY, lengthX);
	
	for( int secondLengthY = 0; secondLengthY < lengthY; secondLengthY++) {
		for( int secondLengthX = 0; secondLengthX < lengthX; secondLengthX++) {
			int currentCoordinate = calculateIndex( secondLengthX, secondLengthY);
			Thing* newObj = obj.getObjByCoordinates(secondLengthX, secondLengthY);
			this->setObjToMatrix( currentCoordinate, newObj->getCopyOfObject());
		}
	}
	return *this;
}
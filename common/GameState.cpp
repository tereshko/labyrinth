#include "GameState.h"
#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "DoorKey.h"

GameState::GameState():m_maze(4,4),m_userProfile(0,0) {
	int fd;
	const char* fileName = "editorSave";
	fd = openFile( fileName, false);
	if (fd != -1) {
		m_maze.loadMaze( fd);
		m_userProfile.loadUserProfile( fd);
	}
}

int GameState::openFile( const char* fileName, bool isForSave) {
	int fd;
	fd = open( fileName, O_RDWR);
	if( fd == -1) {
		if( isForSave) {
			fd = createFile( fileName);
		}
	}
	return fd;
}

int GameState::createFile( const char* fileName) {
	int fd;
	fd = open( fileName, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	return fd;
}

Coordinats GameState::getCoordinatesToMove( SideOfWorld moveSide) const {
	Coordinats coordinatsToMove;
	int userY = m_userProfile.m_userCoordinats.coordinatY;
	int userX = m_userProfile.m_userCoordinats.coordinatX;
	switch (moveSide) {
		case SIDE_NORTH: {
			userY -= 1;
			coordinatsToMove.coordinatX = userX;
			coordinatsToMove.coordinatY = userY;
			break;
		}
		case SIDE_EAST: {
			userX += 1;
			coordinatsToMove.coordinatX = userX;
			coordinatsToMove.coordinatY = userY;
			break;
		}
		case SIDE_SOUTH: {
			userY += 1;
			coordinatsToMove.coordinatX = userX;
			coordinatsToMove.coordinatY = userY;
			break;
		}
		case SIDE_WEST: {
			userX -= 1;
			coordinatsToMove.coordinatX = userX;
			coordinatsToMove.coordinatY = userY;
			break;
		}
		default:
		break;
	}
	return coordinatsToMove;
}

bool GameState::doStep( SideOfWorld sideOfWorld, TypeOfUserActionsForMove actionTypeForMove, int inventoryId) {
	bool isUserMoved = false;
	Coordinats coordinatsWhereUserWantsToMove;
	
	coordinatsWhereUserWantsToMove = getCoordinatesToMove( sideOfWorld);

	int userToMoveX = coordinatsWhereUserWantsToMove.coordinatX;
	int userToMoveY = coordinatsWhereUserWantsToMove.coordinatY;

	bool isStepValid = false;
	isStepValid = isMoveValid( userToMoveX, userToMoveY);

	if( actionTypeForMove == MOVE) {
		if( isStepValid) {
			m_userProfile.m_userCoordinats.coordinatY = userToMoveY;
			m_userProfile.m_userCoordinats.coordinatX = userToMoveX;
			isUserMoved = true;
		}
	}

	if( actionTypeForMove == OPEN_DOOR) {
		bool isDoorOpen = applyInventoryToCoordinates( inventoryId, userToMoveX, userToMoveY);
		if( isDoorOpen){
			m_userProfile.m_userCoordinats.coordinatY = userToMoveY;
			m_userProfile.m_userCoordinats.coordinatX = userToMoveX;
			isUserMoved = true;
		}
		
	}
	return isUserMoved;
}

//TODO: тут door нужно сделать проверку или убрать ее
bool GameState::isCoordinatesExit() {
	bool isExit = false;

	Thing* doorObj = nullptr;
	doorObj = m_maze.getObjByCoordinates( m_userProfile.m_userCoordinats.coordinatX, m_userProfile.m_userCoordinats.coordinatY);

	if( doorObj != nullptr) {
		if( doorObj->getType() == CellValues::DOOR){
			isExit = doorObj->canPass();
		}
		if( doorObj->getType() == CellValues::EXIT){
			isExit = true;
		}
	}
	return isExit;
}

bool GameState::isMoveValid( int userX, int userY) {
	bool canPass = false;
	Thing* obj = nullptr;
	obj = m_maze.getObjByCoordinates( userX, userY);
	if( obj != nullptr) {
		canPass = obj->canPass();
		if( obj->canPickUp()) {
			m_userProfile.addInventoryObj(obj);
			setMatrixValueByCoordinats( userY, userX, CellValues::EMPTY);
		}
	}
	return canPass;
}

const UserProfile& GameState::getUserProfile() {
	return m_userProfile;
}

const Maze& GameState::getMaze() {
	return m_maze;
}

bool GameState::isCoordinatesDoor( int x, int y) const {
	bool isDoor = false;
	CellValues cellValue;
	cellValue = m_maze.getCellValueFromCoordinates(x, y);
	if( cellValue == CellValues::DOOR) {
		isDoor = true;
	}
	return isDoor;
}

bool GameState::isDoorOpened( SideOfWorld moveToSide) const {
	Coordinats whereUserWantToMove;
	whereUserWantToMove = getCoordinatesToMove( moveToSide);
	int userMoveX = whereUserWantToMove.coordinatX;
	int userMoveY = whereUserWantToMove.coordinatY;

	Thing* doorObj = nullptr;
	bool dooIsOpened = false;
	doorObj = m_maze.getObjByCoordinates( userMoveX, userMoveY);
	if( doorObj->getType() == CellValues::DOOR) {
		dooIsOpened = doorObj->canPass();
	}
	return dooIsOpened;
}

int GameState::getMazeLengthY() const {
	return m_maze.getLengthY();
}

int GameState::getMazeLengthX() const {
	return m_maze.getLengthX();
}

void GameState::setMatrixValueByCoordinats( int y, int x, CellValues mazeValue) {
	m_maze.setMatrixValue( y, x, mazeValue);
}

bool GameState::isMazeValueValid( int value) const {
	CellValues mazeValueCorrect = (CellValues)value;
	return m_maze.isValueValid( mazeValueCorrect);
}

bool GameState::isMazeLengthYValid( int lengthY) const {
	return m_maze.isLengthYValid( lengthY);
}

bool GameState::isMazeLengthXValid( int lengthX) const {
	return m_maze.isLengthXValid( lengthX);
}

bool GameState::applyInventoryToCoordinates( int inventoryNumer, int userX, int userY) {
	bool doorIsOpened = false;
	Thing* obj = nullptr;
	obj = m_userProfile.getInventoryByIndex( inventoryNumer);
	if( obj != nullptr) {
		Thing* doorObj = nullptr;
		doorObj = m_maze.getObjByCoordinates( userX, userY);
		doorIsOpened = doorObj->apply( obj);
		if( doorIsOpened) {
			m_userProfile.removeInventoryByIndex( inventoryNumer);
		}
	}
	return doorIsOpened;
}

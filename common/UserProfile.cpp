#include "UserProfile.h"
#include <iostream>
#include "Colors.h"
#include "Door.h"
#include "DoorKey.h"

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

UserProfile::UserProfile( int coordinatX, int coordinatY) {
	m_firstItemNodeObj = nullptr;
	setUserCoordinates( coordinatX, coordinatY);
}

UserProfile::~UserProfile() {
	InventoryObj* current = m_firstItemNodeObj;
	InventoryObj* next = nullptr;
	while (current != nullptr) {
		next = current->next;
		delete current;
		current = next;
	}
}

UserProfile& UserProfile::operator=( const UserProfile &obj) {
	this->setUserCoordinates( obj.getUserCoordinats().coordinatX, obj.getUserCoordinats().coordinatY);
	int inventoryCount = obj.getCountOfInventory();
	
	if( inventoryCount > -1) {
		int invCn = this->getCountOfInventory();
		while (invCn > -1) {
			this->removeInventoryByIndex( invCn);
			invCn --;
		}
		Thing* inventoryObj = nullptr;
		for (int i = 0; i <= inventoryCount; i++) {
			inventoryObj = obj.getInventoryByIndex(i);
			this->addInventoryObj( inventoryObj);
		}
	}
	return *this;
}

UserProfile::InventoryObj* UserProfile::createInventoryObjNode( Thing* obj) {
	InventoryObj* newNode = new InventoryObj;
	newNode->obj = obj;
	newNode->next = nullptr;
	return  newNode;
}

void UserProfile::addInventoryObj(Thing *obj) {
	InventoryObj* newUserNode = nullptr;
	newUserNode = createInventoryObjNode( obj);

	InventoryObj* temp = nullptr;

	if( m_firstItemNodeObj == nullptr){
		temp = newUserNode;
	} else{
		temp = m_firstItemNodeObj;
		while( m_firstItemNodeObj->next != nullptr) {
			m_firstItemNodeObj = m_firstItemNodeObj->next;
		}
		m_firstItemNodeObj->next = newUserNode;
	}
	m_firstItemNodeObj = temp;
}

Thing* UserProfile::getInventoryByIndex( int index) const {
	int nodesObjCount = getCountOfInventory();
	InventoryObj* currentNode = nullptr;
	currentNode = m_firstItemNodeObj;
	Thing* inventoryObj = nullptr;
	if(( index < 0) || (index > nodesObjCount)) {
		inventoryObj = nullptr;
	} else {
		if( currentNode->next == nullptr){
			inventoryObj = currentNode->obj;
		} else {
			int i = 0;
			while( i != index){
				currentNode = currentNode->next;
				i++;
			}
			inventoryObj = currentNode->obj;
		}
	}
	return inventoryObj;
}

int UserProfile::getCountOfInventory() const {
	int nodesCount = -1;
	if( m_firstItemNodeObj != nullptr){
		InventoryObj* currentNode = nullptr;
		currentNode = m_firstItemNodeObj;
		if( currentNode->next == nullptr){
			nodesCount = 0;
		} else{
			nodesCount = 0;
			while(currentNode->next != nullptr) {
				nodesCount++;
				currentNode = currentNode->next;
			}
		}
	}
	return nodesCount;
}

void UserProfile::removeInventoryByIndex(int index) {
	InventoryObj* newHead = nullptr;

	if( m_firstItemNodeObj == nullptr){
		newHead = nullptr;
	} else {
		if (index == 0) {
			newHead = m_firstItemNodeObj->next;
			free( m_firstItemNodeObj);
		} else {
			newHead = m_firstItemNodeObj;
			InventoryObj* nodeForDelete = nullptr;
			for( int i = 1; i < index; i++) {
				m_firstItemNodeObj = m_firstItemNodeObj->next;
			}
			nodeForDelete = m_firstItemNodeObj;
			nodeForDelete = nodeForDelete->next;
			m_firstItemNodeObj->next = nodeForDelete->next;
			free( nodeForDelete);
		}
	}
	m_firstItemNodeObj = newHead;
}

UserProfile::InventoryObj* UserProfile::getFirstInventory() const {
	return m_firstItemNodeObj;
}

Coordinats UserProfile::getUserCoordinats() const {
	Coordinats coordinats;
	coordinats.coordinatX = m_userCoordinats.coordinatX;
	coordinats.coordinatY = m_userCoordinats.coordinatY;
	return coordinats;
}

void UserProfile::saveUserProfile( int fd) const {
	int coordinateX = m_userCoordinats.coordinatX;
	write( fd, &coordinateX, sizeof (int));

	int coordinateY = m_userCoordinats.coordinatY;
	write( fd, &coordinateY, sizeof (int));

	int inventoryCount = getCountOfInventory();
	write( fd, &inventoryCount, sizeof (int));
	for (int i = 0; i <= inventoryCount; i++) {
		Thing* inventoryObj = nullptr;
		CellValues typeOfInventory;
		inventoryObj = getInventoryByIndex( i);
		typeOfInventory = inventoryObj->getType();
		write( fd, &typeOfInventory, sizeof (CellValues));
		if( typeOfInventory == DOOR_KEY){
			DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(inventoryObj);
			Colors color;
			color = objForDoorKeyColor->getColor();
			write( fd, &color, sizeof(Colors));
		}
	}
}

UserProfile UserProfile::getUserProfile( int fd) {
	UserProfile userProfile(0,0);
	int coordinateX, coordinateY, inventoryCount;
	read( fd, &coordinateX, sizeof (int));
	read( fd, &coordinateY, sizeof (int));
	read( fd, &inventoryCount, sizeof (int));
	userProfile.setUserCoordinates( coordinateX, coordinateY);

	Thing* inventoryObj = nullptr;
	CellValues typeOfInventory;
	Colors colorOfObject;
	for (int i = 0; i <= inventoryCount; i++) {
		read( fd, &typeOfInventory, sizeof (CellValues));
		read( fd, &colorOfObject, sizeof( Colors));
		if( typeOfInventory == DOOR_KEY) {
			TypesMazeObjects typeMazeObject;
			inventoryObj = typeMazeObject.getTypeMazeObject( typeOfInventory);
			DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(inventoryObj);
			objForDoorKeyColor->setColor( colorOfObject);
			userProfile.addInventoryObj( objForDoorKeyColor);
		}
	}
	return userProfile;
}

void UserProfile::loadUserProfile( int fd) {
	int coordinateX, coordinateY, inventoryCount;
	read( fd, &coordinateX, sizeof (int));
	read( fd, &coordinateY, sizeof (int));
	read( fd, &inventoryCount, sizeof (int));
	setUserCoordinates( coordinateX, coordinateY);
	
	int invCn = getCountOfInventory();
	while (invCn > -1) {
		removeInventoryByIndex( invCn);
		invCn --;
	}

	Thing* inventoryObj = nullptr;
	CellValues typeOfInventory;
	Colors colorOfObject;
	for (int i = 0; i <= inventoryCount; i++) {
		read( fd, &typeOfInventory, sizeof (CellValues));
		read( fd, &colorOfObject, sizeof( Colors));
		if( typeOfInventory == DOOR_KEY) {
			inventoryObj = m_typeMazeObject.getTypeMazeObject( typeOfInventory);
			DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(inventoryObj);
			objForDoorKeyColor->setColor( colorOfObject);
			addInventoryObj( objForDoorKeyColor);
		}
	}
}

void UserProfile::setUserCoordinates( int coordinateX, int coordinateY) {
	m_userCoordinats.coordinatX = coordinateX;
	m_userCoordinats.coordinatY = coordinateY;
}

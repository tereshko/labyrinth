#include <TypesMazeObjects.h>
#include <iostream>

#include "Empty.h"
#include "Wall.h"
#include "Door.h"
#include "DoorKey.h"
#include "Exit.h"

Thing* TypesMazeObjects::getTypeMazeObject( CellValues value){
	Thing* obj = nullptr;
	switch (value) {
		case EMPTY:{
			obj = new Empty;
			break;}
		case WALL:{
			obj = new Wall;
			break;}
		case DOOR:{
			obj = new Door;
			break;}
		case DOOR_KEY:{
			obj = new DoorKey;
			break;}
		case EXIT:{
			obj = new Exit;
			break;}
		default:{
			obj = nullptr;
			break;}
		}
	return obj;
}
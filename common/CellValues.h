#ifndef H_CELLVALUES
#define H_CELLVALUES

enum CellValues {
	EMPTY,
	WALL,
	DOOR,
	DOOR_KEY,
	EXIT,
	OUT_OF_MAZE
};

#endif
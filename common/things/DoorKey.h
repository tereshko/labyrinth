#ifndef H_DOORKEY
#define H_DOORKEY

#include "Thing.h"
#include "Colors.h"

class DoorKey : public Thing {

public:
	DoorKey();
	void draw() override;
	void drawName() override;
	CellValues getType() override;
	bool apply( Thing* thing) override;
	bool canPass() override;
	bool canPickUp() override;
	Thing* getCopyOfObject() override;

	Colors getColor();
	void setColor(Colors color);

	void setColorForDoorKey();

private:
	Colors selectColor();
	Colors m_color;
};
#endif
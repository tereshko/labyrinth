#ifndef H_THING
#define H_THING

#include "CellValues.h"

class Thing {

public:
	virtual ~Thing() = default;
	virtual void draw() =0;
	virtual void drawName() =0;
	virtual CellValues getType() = 0;
	virtual bool apply( Thing* thing) = 0;
	virtual bool canPass() = 0;
	virtual bool canPickUp() = 0;
	virtual Thing* getCopyOfObject() = 0;

};

#endif


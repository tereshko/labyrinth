#include "DoorKey.h"
#include<iostream>

DoorKey::DoorKey() {
}

void DoorKey::draw(){
	std::cout << CellValues::DOOR_KEY;
}

void DoorKey::drawName(){
	std::cout << "key";
}

CellValues DoorKey::getType() {
	return CellValues::DOOR_KEY;
}

bool DoorKey::apply(Thing* thing){
	return false;
}

bool DoorKey::canPass(){
	return true;
}

bool DoorKey::canPickUp(){
	return true;
}

Colors DoorKey::getColor(){
	return m_color;
}


Colors DoorKey::selectColor(){
	int selectedColor;
	bool isExit = false;
	while( !isExit) {
		std::cout << Colors::BLACK << " Black\n";
		std::cout << Colors::GREEN << " Green\n";
		std::cout << Colors::RED << " Red\n";
		std::cout << "Please select color: ";
		std::cin >> selectedColor;
		if( selectedColor >= Colors::BLACK && selectedColor <= Colors::RED ) {
			isExit = true;
		} else {
			std::cout << "\nPlease select color between " << Colors::BLACK << " and " << Colors::RED  << "\n";
		}
	}
	return Colors(selectedColor);
}

void DoorKey::setColor(Colors color) {
	m_color = color;
}

void DoorKey::setColorForDoorKey(){
	Colors color;
	color = selectColor();
	setColor( color);
}

Thing* DoorKey::getCopyOfObject() {
	DoorKey* doorKey = new DoorKey;
	doorKey->setColor( this->getColor());
	return doorKey;
}

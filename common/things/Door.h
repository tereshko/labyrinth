#ifndef H_DOOR
#define H_DOOR

#include "Thing.h"
#include "Colors.h"

class Door : public Thing {

public:
	Door();
	void draw() override;
	void drawName() override;
	CellValues getType() override;
	bool apply( Thing* thing) override;
	bool canPass() override;
	bool canPickUp() override;
	Thing* getCopyOfObject() override;

	Colors getColor();
	void setColor(Colors color);
	void setPass(bool canPass);
	void setColorForDoor();
private:
	bool m_open;
	Colors m_color;
	Colors selectColor();
};
#endif
#ifndef H_WALL
#define H_WALL

#include "Thing.h"

class Wall : public Thing {

public:
	void draw() override;
	void drawName() override;
	CellValues getType() override;
	bool apply( Thing* thing) override;
	bool canPass() override;
	bool canPickUp() override;
	Thing* getCopyOfObject() override;



};


#endif
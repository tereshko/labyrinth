#ifndef H_EXIT
#define H_EXIT

#include "Thing.h"

class Exit : public Thing {

public:
	void draw() override;
	void drawName() override;
	CellValues getType() override;
	bool apply( Thing* thing) override;
	bool canPass() override;
	bool canPickUp() override;
	
	Thing* getCopyOfObject() override;


};


#endif
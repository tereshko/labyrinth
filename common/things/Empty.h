#ifndef H_EMPTY
#define H_EMPTY

#include "Thing.h"

class Empty : public Thing {

public:
	void draw() override;
	void drawName() override;
	CellValues getType() override;
	bool apply( Thing* thing) override;
	bool canPass() override;
	bool canPickUp() override;
	Thing* getCopyOfObject() override;
};


#endif
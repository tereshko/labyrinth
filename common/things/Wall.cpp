#include "Wall.h"
#include <iostream>


void Wall::draw(){
	std::cout << CellValues::WALL;
}

void Wall::drawName(){
	std::cout << "wall";
}

CellValues Wall::getType() {
	return CellValues::WALL;
}

bool Wall::apply(Thing *thing){
	return false;
}

bool Wall::canPass(){
	return false;
}

bool Wall::canPickUp(){
	return false;
}

Thing* Wall::getCopyOfObject() {
	Wall* wall = new Wall;
	return wall;
	
}
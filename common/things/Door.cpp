#include "Door.h"
#include "DoorKey.h"
#include<iostream>

Door::Door(){
	m_open = false;
}

void Door::draw(){
	std::cout << CellValues::DOOR;
}

void Door::drawName(){
	std::cout << "door";
}

CellValues Door::getType() {
	return CellValues::DOOR;
}

bool Door::canPass(){
	return m_open;
}

bool Door::canPickUp(){
	return false;
}

bool Door::apply(Thing* thing){
	DoorKey* doorKey = dynamic_cast<DoorKey*>(thing);
	if( doorKey){
		if (doorKey->getColor() == m_color) {
			m_open = true;
		}
	} else {
		m_open = false;
	}
	return m_open;
}

Colors Door::getColor(){
	return m_color;
}

Colors Door::selectColor(){
	int selectedColor;
	bool isExit = false;
	while( !isExit) {
		std::cout << Colors::BLACK << " Black\n";
		std::cout << Colors::GREEN << " Green\n";
		std::cout << Colors::RED << " Red\n";
		std::cout << "Please select color: ";
		std::cin >> selectedColor;
		if( selectedColor >= Colors::BLACK && selectedColor <= Colors::RED ) {
			isExit = true;
		} else {
			std::cout << "\nPlease select color between " << Colors::BLACK << " and " << Colors::RED << "\n";
		}
	}
	return Colors(selectedColor);
}

void Door::setColor(Colors color) {
	m_color = color;
}

void Door::setPass(bool canPass) {
	m_open = canPass;
}

void Door::setColorForDoor(){
	Colors color;
	color = selectColor();
	setColor( color);
}

Thing* Door::getCopyOfObject() {
	Door* door = new Door;
	door->setColor( this->getColor());
	door->setPass( this->m_open);
	return door;
}


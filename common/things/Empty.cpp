#include "Empty.h"
#include<iostream>


void Empty::draw() {
	std::cout << CellValues::EMPTY;
}

void Empty::drawName(){
	std::cout << "empty";
}

CellValues Empty::getType() {
	return CellValues::EMPTY;
}

bool Empty::apply(Thing *thing){
	return false;
}

bool Empty::canPass(){
	return true;
}

bool Empty::canPickUp(){
	return false;
}

Thing* Empty::getCopyOfObject() {
	Empty* empty = new Empty;
	return empty;
}
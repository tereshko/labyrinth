#include "Exit.h"
#include<iostream>


void Exit::draw() {
	std::cout << CellValues::EXIT;
}

void Exit::drawName(){
	std::cout << "exit";
}

CellValues Exit::getType() {
	return CellValues::EXIT;
}

bool Exit::apply(Thing *thing){
	return false;
}

bool Exit::canPass(){
	return true;
}

bool Exit::canPickUp(){
	return false;
}

Thing* Exit::getCopyOfObject() {
	Exit* exit = new Exit;
	return exit;
	
}
#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "IGame.h"
#include "Colors.h"

class GameState : public IGame{
public:
	GameState();

	bool isCoordinatesExit();

	bool isMoveValid( int userX, int userY);

	bool doStep( SideOfWorld sideOfWorld, TypeOfUserActionsForMove actionTypeForMove, int inventoryId) override;
	const UserProfile& getUserProfile() override;
	const Maze& getMaze() override;
	
	int getMazeLengthY() const;
	int getMazeLengthX() const;
	
	void setMatrixValueByCoordinats( int y, int x, CellValues value);

	bool isMazeValueValid( int value) const;
	bool isMazeLengthYValid( int lengthY) const;
	bool isMazeLengthXValid( int lengthX) const;
	
	bool isCoordinatesDoor( int x, int y) const;
	Coordinats getCoordinatesToMove( SideOfWorld moveSide) const;
	bool isDoorOpened( SideOfWorld moveToSide) const;

private:
	int openFile( const char* fileName, bool isForSave);
	int createFile( const char* fileName);

	bool applyInventoryToCoordinates( int inventoryNumer, int userX, int userY);
	UserProfile m_userProfile;
	Maze m_maze;
	
	bool isLengthYValid( int lengthX, int lengthXMax);
	bool isLengthXValid( int lengthX, int lengthXMax);

};


#endif
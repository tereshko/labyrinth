#ifndef USERPROFILE_H
#define USERPROFILE_H
#include "Coordinats.h"
#include "TypesMazeObjects.h"

class Thing;

class UserProfile
	{
public:
	UserProfile( int coordinatX, int coordinatY);
	~UserProfile();
	UserProfile& operator=( const UserProfile &obj);

	Coordinats m_userCoordinats;

	struct InventoryObj {
		Thing* obj;
		InventoryObj* next;
	};

	void addInventoryObj( Thing* obj);
	int getCountOfInventory() const;
	Thing* getInventoryByIndex( int index) const;
	void removeInventoryByIndex( int index);
	InventoryObj* getFirstInventory() const;
	Coordinats getUserCoordinats() const;
	void saveUserProfile( int fd) const;
	void loadUserProfile( int fd);
	void setUserCoordinates( int coordinateX, int coordinateY);
	static UserProfile getUserProfile( int fd);

private:
	TypesMazeObjects m_typeMazeObject;
	InventoryObj* m_firstItemNodeObj;

	InventoryObj* createInventoryObjNode( Thing* obj);
};

#endif
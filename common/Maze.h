#ifndef MAZE_H
#define MAZE_H

#include "CellValues.h"
#include "TypesMazeObjects.h"

class Thing;

class Maze {

public:
	Maze( int lengthY, int lengthX);
	~Maze();
	Maze& operator=( const Maze &obj);

	void resize( int lengthY, int lengthX);
	void saveMaze(int fd) const;
	void loadMaze(int fd);
	int getLengthY() const;
	int getLengthX() const ;
	void setMatrixValue( int y, int x, CellValues value);

	bool isLengthYValid( int lengthY) const;
	bool isLengthXValid( int lengthX) const;
	bool isValueValid( CellValues value) const;

	Thing* getObjByCoordinates( int x, int y) const;
	CellValues getCellValueFromCoordinates( int x, int y) const;
	void setObjToMatrix( int matrixNumber, Thing* obj);
	static Maze getMaze( int fd);

private:
	TypesMazeObjects m_typeMazeObject;
	int m_lengthX;
	int m_lengthY;
	Thing** m_cell;
	bool isEnteredDataValid( int lengthY, int lengthX, CellValues value);
	int calculateIndex( int coordinateX, int coordinateY);
};
#endif

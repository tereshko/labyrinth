#ifndef NETWORKGAME_H
#define NETWORKGAME_H

#include "IGame.h"
#include "Colors.h"

class NetworkGame : public IGame {

public:
	NetworkGame();
	~NetworkGame();

	bool doStep( SideOfWorld sideOfWorld, TypeOfUserActionsForMove actionTypeForMove, int inventoryId) override;
	const UserProfile& getUserProfile() override;
	const Maze& getMaze() override;

private:
	bool connection();
	void sendDostep( SideOfWorld sideOfWorld, TypeOfUserActionsForMove actionTypeForMove, int inventoryId);

	int m_sock;
	Maze m_maze;
	UserProfile m_userProfile;

};

#endif
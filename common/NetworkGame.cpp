#include "NetworkGame.h"

#include "Maze.h"
#include "Thing.h"
#include "Empty.h"
#include "Wall.h"
#include "Door.h"
#include "DoorKey.h"
#include "Exit.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>

NetworkGame::NetworkGame():m_maze(0,0), m_userProfile(0,0) {
	m_sock = -1;
	bool isConnected = connection();
	if( isConnected) {
		m_maze = Maze::getMaze( m_sock);
		m_userProfile = UserProfile::getUserProfile( m_sock);
	}
}

NetworkGame::~NetworkGame() {
	close( m_sock);
}

bool NetworkGame::connection() {
	bool isConnected = false;
	struct sockaddr_in addr;
	m_sock = socket(AF_INET, SOCK_STREAM, 0);
	if(m_sock > 0) {
		addr.sin_family = AF_INET;
		addr.sin_port = htons(PORT_USAGE);
		addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
		if((inet_pton(addr.sin_family, "68.183.140.192", &addr.sin_addr)) <= 0){
			printf("\nConnection Failed \n"); 
			return -1; 
		}
		if(connect(m_sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
			std::cout << "error. Can't open socket to connect\n";
		} else {
			isConnected = true;
		}
	} else {
		std::cout << "error. Can't create socket\n";
	}
	return isConnected;
}

bool NetworkGame::doStep(SideOfWorld sideOfWorld, TypeOfUserActionsForMove actionTypeForMove, int inventoryId) {
	sendDostep( sideOfWorld, actionTypeForMove, inventoryId);
	m_maze = Maze::getMaze( m_sock);
	m_userProfile = UserProfile::getUserProfile( m_sock);
}

const UserProfile& NetworkGame::getUserProfile() {
	return m_userProfile;
}

const Maze& NetworkGame::getMaze() {
	return m_maze;
}

void NetworkGame::sendDostep( SideOfWorld sideOfWorld, TypeOfUserActionsForMove actionTypeForMove, int inventoryId) {
	write( m_sock, &sideOfWorld, sizeof ( SideOfWorld));
	write( m_sock, &actionTypeForMove, sizeof ( TypeOfUserActionsForMove));
	write( m_sock, &inventoryId, sizeof ( int));
}

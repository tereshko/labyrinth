#ifndef TYPESMAZEOBJECTS_H
#define TYPESMAZEOBJECTS_H
#include "Thing.h"
#include "CellValues.h"

class TypesMazeObjects {
public:
	Thing* getTypeMazeObject( CellValues value);
};

#endif
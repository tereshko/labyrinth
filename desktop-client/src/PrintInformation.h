#ifndef PRINTINFORMATION_H
#define PRINTINFORMATION_H

class PrintInformation
	{
public:
	int takeUserAction();
	int getUserActionForMenuStartGame();
	int getUserActionForMenuEditMaze();
	void showMazeValues();
	void showTypeOfUserMove();

private:
	
	};

#define ACTION__START_GAME 1
#define ACTION__EDIT_MAZE 2
#define ACTION__SAVE 3
#define ACTION__UPLOAD_SAVE 4
#define ACTION__EXIT 5

#define ACTION__DO_STEP 1
#define ACTION__SHOW_INVENTORY 2
#define ACTION__EXIT_FROM_MENU_DOSTEP 3

#define ACTION__RESIZE_MAZE 1
#define ACTION__CHANGE_MAZE 2
#define ACTION__MOVE_USER 3
#define ACTION__EXIT_FROM_MENU_MAZEACTIONS 4

#endif // PRINTINFORMATION_H
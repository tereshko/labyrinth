#ifndef GAME_H
#define GAME_H

#include "PrintInformation.h"
#include "Colors.h"
#include "IGame.h"
#include <string>

class Engine {

public:
	Engine( IGame* iGame);
	bool edit();
	bool run();

	void localGameRun();

private:
	void printMatrix();
	void showInventory();
	void printInformationAboutStatusCell( Thing* thing);
	void printInformationAround();
	void printInformationColorOfObject( Colors color);
	SideOfWorld getSideOfWorldToMove();
	bool canGoIfDoor( Coordinats coordinatsToMove);

	bool doStepforDoorWithInventory( SideOfWorld sideToMove);
	
	IGame* m_igame;
	PrintInformation m_printInformation;
	
	std::string askFileName();
	bool applyAction( int userAction);
	TypeOfUserActionsForMove getTypeOfUserActionToMove();

};

#endif

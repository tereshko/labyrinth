#include "Engine.h"
#include <iostream>
#include "DoorKey.h"
#include "GameState.h"

Engine::Engine( IGame* iGame) {
	m_igame = iGame;
}

bool Engine::run(){
	bool isCloseGame = false;
	bool isExitFromMenu = false;
	while(!isExitFromMenu){
		printInformationAround();

		int userAction = m_printInformation.getUserActionForMenuStartGame();
		switch (userAction) {
		case ACTION__DO_STEP:
		{
			SideOfWorld sideToMove;
			TypeOfUserActionsForMove typeOfuserMove;
			
			sideToMove = getSideOfWorldToMove();
			typeOfuserMove = getTypeOfUserActionToMove();
			if( typeOfuserMove == OPEN_DOOR){
				int inventoryCount = m_igame->getUserProfile().getCountOfInventory();
				if( inventoryCount == -1) {
					std::cout << "Inventory is empty. You can only move Move\n";
				} else {
					showInventory();
					int inventoryNumber;
					bool isInventoryNumberValid = false;
					while( !isInventoryNumberValid){
						std::cout << "For open the door, select item from inventory: ";
						std::cin >> inventoryNumber;
						isInventoryNumberValid = (inventoryNumber >= 0) && ( inventoryNumber <= inventoryCount);
					}
					m_igame->doStep( sideToMove, typeOfuserMove, inventoryNumber);
				}
			} else {
				m_igame->doStep( sideToMove, typeOfuserMove, -1);
			}
			
//			coordinatsToMove = m_gameState->getCoordinatesToMove( sideToMove);

//			bool isDoor = m_gameState->isCoordinatesDoor( coordinatsToMove.coordinatX, coordinatsToMove.coordinatY);

//			if( isDoor){
//				if( m_gameState->isDoorOpened( sideToMove)){
//					isCloseGame = m_gameState->doStep( sideToMove, MOVE, -1);
//				} else {
//					isCloseGame = doStepforDoorWithInventory( sideToMove);
//				}
//			}else {
//				if( m_gameState->doStep( sideToMove, MOVE, -1)){
//					isCloseGame = m_gameState->isCoordinatesExit();
//				} else {
//					std::cout << "I can't move\n";
//				}
//			}
			if( isCloseGame){
				isExitFromMenu = true;
			}
		}
			break;
		case ACTION__SHOW_INVENTORY:
			showInventory();
			break;
		case ACTION__EXIT_FROM_MENU_DOSTEP:
			isExitFromMenu = true;
			break;
		default:
			std::cout << "unknown block\n";
			break;
		}
	}
	if(isCloseGame){
		std::cout << "Sorry, this is was exit. Poka\n";
	}
	return isCloseGame;
}

TypeOfUserActionsForMove Engine::getTypeOfUserActionToMove(){
	std::cout << "\n---------------Type For Move---------------\n";
	TypeOfUserActionsForMove moveType;
	bool isSelectCorrect = false;
	int userSelect;
	
	while( !isSelectCorrect){
		m_printInformation.showTypeOfUserMove();
		std::cout << "Please, select action: ";
		std::cin >> userSelect;
		if( userSelect >= TypeOfUserActionsForMove::OPEN_DOOR && userSelect <= TypeOfUserActionsForMove::MOVE) {
			isSelectCorrect = true;
		}
	}
	
	moveType = (TypeOfUserActionsForMove)userSelect;
	std::cout << "-------------------------------------------\n";
	return moveType;
}

bool Engine::doStepforDoorWithInventory( SideOfWorld sideToMoveUser){
	bool canPass = false;
	int nodesObjCount = m_igame->getUserProfile().getCountOfInventory();

	if( nodesObjCount > -1) {
		std::cout << "Door is closed. We will try to open\n";
		int inventoryNumber = -1;

		bool isInventoryNumberValid = false;
		showInventory();
		while( !isInventoryNumberValid){
			std::cout << "For open the door, select item from inventory: ";
			std::cin >> inventoryNumber;
			isInventoryNumberValid = (nodesObjCount >= 0) && ( inventoryNumber <= nodesObjCount);
		}
		canPass = m_igame->doStep( sideToMoveUser, OPEN_DOOR, inventoryNumber);
		if( !canPass){
			std::cout << "Wrong key\n";
		}
	} else {
		std::cout << "Inventory is empty. Find the key\n";
		canPass = false;
	}
	return canPass;
}

bool Engine::edit(){
	bool isCloseGame = false;
	bool isExitFromMenu = false;
	while(!isExitFromMenu){
		printMatrix();

		int userAction = m_printInformation.getUserActionForMenuEditMaze();

		switch (userAction) {
		case ACTION__RESIZE_MAZE:
			std::cout << "No actions\n";
//			resizeMaze();
			break;
		case ACTION__CHANGE_MAZE:
//			changeMaze();
			break;
		case ACTION__MOVE_USER:
			std::cout << "No actions\n";
//			isCloseGame = moveUser();
			if( isCloseGame){
				isExitFromMenu = true;
			}
			break;
		case ACTION__EXIT_FROM_MENU_MAZEACTIONS:
			isExitFromMenu = true;
			break;
		default:
			std::cout << "unknown block\n";
			break;
		}
	}
	return isCloseGame;
}

void Engine::printInformationColorOfObject( Colors color) {
	switch (color) {
		case Colors::BLACK:
			std::cout << "black\n";
			break;
		case Colors::GREEN:
			std::cout << "green\n";
			break;
		case Colors::RED:
			std::cout << "red\n";
			break;
		default:
			std::cout << "unknown color\n";
			break;
	}
}

SideOfWorld Engine::getSideOfWorldToMove() {
	SideOfWorld worldSide;
	int userAction = 0;
	bool correctUserAction = false;

	while (!correctUserAction) {
		std::cout << SIDE_NORTH << ". NORTH\n";
		std::cout << SIDE_EAST << ". EAST\n";
		std::cout << SIDE_SOUTH <<". SOUTH\n";
		std::cout << SIDE_WEST <<". WEST\n";
		std::cout << "Please, select direction: ";
		std::cin >> userAction;
		correctUserAction = (SIDE_NORTH <= userAction) && (SIDE_WEST >= userAction);
	}
	
	worldSide = (SideOfWorld)userAction;
	return worldSide;
}

void Engine::printInformationAboutStatusCell( Thing* thing){
	if ( thing == nullptr){
		std::cout << "border";
	} else {
		thing->drawName();
	}
}

void Engine::printInformationAround() {
	std::cout << "----------------Position-------------------\n";
	Thing* obj;
	
	obj = m_igame->getMaze().getObjByCoordinates( m_igame->getUserProfile().m_userCoordinats.coordinatX, m_igame->getUserProfile().m_userCoordinats.coordinatY - 1);
	std::cout << "\n at north: ";
	printInformationAboutStatusCell( obj);

	obj = m_igame->getMaze().getObjByCoordinates( m_igame->getUserProfile().m_userCoordinats.coordinatX + 1, m_igame->getUserProfile().m_userCoordinats.coordinatY);
	std::cout << "\n at east: ";
	printInformationAboutStatusCell( obj);

	obj = m_igame->getMaze().getObjByCoordinates( m_igame->getUserProfile().m_userCoordinats.coordinatX, m_igame->getUserProfile().m_userCoordinats.coordinatY + 1);
	std::cout << "\n at south: ";
	printInformationAboutStatusCell( obj);

	obj = m_igame->getMaze().getObjByCoordinates( m_igame->getUserProfile().m_userCoordinats.coordinatX - 1, m_igame->getUserProfile().m_userCoordinats.coordinatY);
	std::cout << "\n at west: ";
	printInformationAboutStatusCell( obj);

	std::cout << "\n-------------------------------------------\n";
}

void Engine::showInventory() {
	std::cout << "In inventory we have:\n";
	UserProfile::InventoryObj* currentNode;
	currentNode = m_igame->getUserProfile().getFirstInventory();
	if( currentNode == nullptr){
		std::cout << "The inventory list is empty\n";
	} else {
		int invCount = 0;
		Thing* obj;
		CellValues cellValue;
		while ( currentNode != nullptr) {
			obj = currentNode->obj;
			cellValue = obj->getType();

			std::cout << invCount <<" inventory: ";
			printInformationAboutStatusCell( obj);

			if( cellValue == CellValues::DOOR_KEY) {
				Colors colorInObj;
				DoorKey* objForDoorKeyColor = dynamic_cast<DoorKey*>(obj);
				if( objForDoorKeyColor) {
					colorInObj = objForDoorKeyColor->getColor();
					std::cout << ", color: ";
					printInformationColorOfObject( colorInObj);
				}
			}
			currentNode = currentNode->next;
			invCount++;
		}
	}
}

void Engine::printMatrix() {
	std::cout << "----------------Maze-----------------------\n";
	int lengthX = m_igame->getMaze().getLengthX();
	int lengthY = m_igame->getMaze().getLengthY();

	Coordinats userCoordinates;
	userCoordinates = m_igame->getUserProfile().getUserCoordinats();

	int userIndex = userCoordinates.coordinatY * lengthX + userCoordinates.coordinatX;
	for (int y = 0; y < lengthY; y++) {
		for (int x = 0; x < lengthX; x++) {
			if( userIndex == y*lengthX +x) {
				std::cout << "X" << " ";
			} else{
				CellValues cellValue;
				cellValue = m_igame->getMaze().getCellValueFromCoordinates( x, y);
				std::cout << cellValue;
				std::cout << " ";
			}
		}
		std::cout << "\n";
	} 
	std::cout << "\n-------------------------------------------\n";
}

void Engine::localGameRun() {
	for( bool isExitRequested = false; !isExitRequested ; ) {
		int userAction = m_printInformation.takeUserAction();
		isExitRequested = applyAction( userAction);
	}
	
}

bool Engine::applyAction( int userAction) {

	bool isExitRequested = false;

	switch (userAction) {
	case ACTION__START_GAME:
		isExitRequested = run();
		break;
	case ACTION__EDIT_MAZE:
		isExitRequested = edit();
		break;
	case ACTION__SAVE:
	{
		std::string fileName = askFileName();
		const char* convertedFileName = fileName.c_str();
//		m_gameState->save( convertedFileName);
		break;
	}
	case ACTION__UPLOAD_SAVE:
	{
		std::string fileName = askFileName();
		const char* convertedFileName = fileName.c_str();
//		m_gameState->load( convertedFileName);
		break;
	}
	case ACTION__EXIT:
		isExitRequested = true;
		break;
	default:
		std::cout << "Error. Nothing to do\n";
		break;
	}
	return isExitRequested;
}

std::string Engine::askFileName() {
	std::string fileName;
	std::cout << "\nPlease, enter file name: ";
	std::cin >> fileName;
	return fileName;
}




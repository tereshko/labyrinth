#include "Application.h"
#include <iostream>

int main (int argc, char** argv) {

	Application app;
	int result = app.exec();

	return result;
}

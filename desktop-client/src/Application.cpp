#include "Application.h"
#include <iostream>
#include "GameState.h"
#include "Engine.h"
#include "NetworkGame.h"

int Application::exec() {
	bool isExitRequested = false;
	while( !isExitRequested){
		std::cout << "1. Local game\n";
		std::cout << "2. Network game\n";
		std::cout << "3. Exit\n";
		int userSelect;
		std::cout << "Please, select type of game: ";
		std::cin >> userSelect;
		switch (userSelect) {
			case 1: {
				GameState gameState;
				Engine engine( &gameState);
				engine.localGameRun();
				break;
			}
			case 2: {
				NetworkGame networkGame;
				Engine engine( &networkGame);
				engine.localGameRun();
				break;
			}
			case 3: {
				isExitRequested = true;
				break;
			}
			default:
				std::cout << "Incorrect answer.\n";
				break;
			}
	}
	return 0;
}
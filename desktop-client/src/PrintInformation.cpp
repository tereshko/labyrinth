#include "PrintInformation.h"
#include<iostream>

#include "CellValues.h"
#include "TypeOfUserActionsForMove.h"

int PrintInformation::takeUserAction () {

	int userAction = 0;
	bool correctUserAction = false;

	while (!correctUserAction) {
		std::cout << ACTION__START_GAME << ". Start game\n";
		std::cout << ACTION__EDIT_MAZE << ". Edit maze\n";
		std::cout << ACTION__SAVE << ". Save\n";
		std::cout << ACTION__UPLOAD_SAVE << ". Upload from save\n";
		std::cout << ACTION__EXIT << ". Exit\n";
		std::cout << "What you want to do: ";
		std::cin >> userAction;

		correctUserAction = (ACTION__START_GAME <= userAction) && (ACTION__EXIT >= userAction);
	}

	return userAction;
}

int PrintInformation::getUserActionForMenuStartGame(){
	int userAction = 0;
	bool correctUserAction = false;

	while (!correctUserAction) {
		std::cout << ACTION__DO_STEP << ". Do step\n";
		std::cout << ACTION__SHOW_INVENTORY << ". Show inventory\n";
		std::cout << ACTION__EXIT_FROM_MENU_DOSTEP << ". Exit\n";
		std::cout << "What you want to do: ";
		std::cin >> userAction;

		correctUserAction = (ACTION__DO_STEP <= userAction) && (ACTION__EXIT_FROM_MENU_DOSTEP >= userAction);
	}
	return userAction;
}

int PrintInformation::getUserActionForMenuEditMaze(){
	int userAction = 0;
	bool correctUserAction = false;

	while (!correctUserAction) {
		std::cout << ACTION__RESIZE_MAZE << ". Resize maze\n";
		std::cout << ACTION__CHANGE_MAZE << ". Change maze\n";
		std::cout << ACTION__MOVE_USER << ". Move user\n";
		std::cout << ACTION__EXIT_FROM_MENU_MAZEACTIONS << ". Exit\n";
		std::cout << "What you want to do: ";
		std::cin >> userAction;

		correctUserAction = (ACTION__RESIZE_MAZE <= userAction) && (ACTION__EXIT_FROM_MENU_MAZEACTIONS >= userAction);
	}
	return userAction;
}

void PrintInformation::showMazeValues(){
	std::cout << CellValues::EMPTY << ". empty\n";
	std::cout << CellValues::WALL << ". wall\n";
	std::cout << CellValues::DOOR << ". door\n";
	std::cout << CellValues::DOOR_KEY << ". door key\n";
	std::cout << CellValues::EXIT << ". exit\n";
}

void PrintInformation::showTypeOfUserMove(){
	std::cout << TypeOfUserActionsForMove::OPEN_DOOR << ". Open Door\n";
	std::cout << TypeOfUserActionsForMove::MOVE << ". Move\n";
}

